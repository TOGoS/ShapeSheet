export enum DepthOp {
	NONE,
	REPLACE,
	BUILD,
	DIG
}

export enum MaterialOp {
	NONE,
	REPLACE,
	REPLACE_IF_DEPTH_AFFECTED
}

export default class PlotMode {
	constructor(public depthOp:DepthOp, public materialOp:MaterialOp) { } 
	
	public static BUILD = new PlotMode(DepthOp.BUILD, MaterialOp.REPLACE_IF_DEPTH_AFFECTED);
	public static DIG   = new PlotMode(DepthOp.DIG, MaterialOp.NONE);
};
