## Misc to-do items

- Convex polygon-based solids
- Arbitrary brush shapes
- Support cutting through (out the back!) of shapes

- Support .obj format natively;
    urn:asdasd eval-obj
  or
    "v 0 0 1 ... " eval-obj
  Can require that string be a literal or URI and compile it to a forth word at compile-time.
- Allow $material-index to be a (x,y,z) -> material index function
  - Origin/transform determined by context when function defined?
    - which would automatically account for object rotation
For tubes:
- Render front, back half of tubes separately
- For translucent surfaces, dither in shapesheet; will be AAd to translucency when rendered.
