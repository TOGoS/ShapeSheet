# Why do I keep making RDF-SCHEMA.txt instead of ye olde schema.txt?
# Maybe I should resurrect SchemaSchema[Demo] instead of inventing
# more different formats.
# -- TOGoS, 2021-11-29

=type http://ns.nuke24.net/ShapeSheet/Buffer
required-attribute: http://ns.nuke24.net/ShapeSheet/width
required-attribute: http://ns.nuke24.net/ShapeSheet/height
optional-attribute: http://ns.nuke24.net/ShapeSheet/layerCount
	: comment @ "how many layers are represented?"
	: default value @ 1
required-attribute: http://ns.nuke24.net/ShapeSheet/pixelMaterialIndexes
	: type @ array of integer
	: comment @ "array of integers representing indexes into a materials array"
	: default element value @ 0
required-attribute: http://ns.nuke24.net/ShapeSheet/pixelDepths
	: type @ array of number
	: comment @ "array of depths of layer frontsides"
	: default element value @ infinity
required-attribute: http://ns.nuke24.net/ShapeSheet/pixelBackDepths
	: type @ array of number
	: comment @ "array of depths of layer backsides"
	: default element value @ infinity
required-attribute: http://ns.nuke24.net/ShapeSheet/pixelDzOverDx
	: type @ array of number
	: comment @ "array of dz/dx at each pixel's center"
	: default element value @ 0
required-attribute: http://ns.nuke24.net/ShapeSheet/pixelDzOverDy
	: type @ array of number
	: comment @ "array of dz/dy at each pixel's center"
	: default element value @ 0
optional-attribute: http://ns.nuke24.net/ShapeSheet/origin
	: comment @ "offset from the top,left,front corner of the buffer to the logical 'center' or 'foot' of the object"
	: example @ "32, 32, 10"
optional-attribute: http://ns.nuke24.net/ShapeSheet/pixelSize
	: comment @ "width and height represented a pixel"
	: example @ "1.2 mm"
	: example @ "1 mm, 2mm"
optional-attribute: http://ns.nuke24.net/ShapeSheet/depthUnit
	: comment @ "distance indicated by a difference of 1 in the depth buffer"
	: example @ "1/10 in"
optional-attribute: http://ns.nuke24.net/ShapeSheet/materialPalette
	: comment @ "An array of materials"
optional-attribute: http://ns.nuke24.net/ShapeSheet/samplePosition
	: type @ "Center"|"TopLeft"
	: default value @ "Center"
	: comment @ "
		What point within each pixel do the numbers represent?
		Center is probably the most intuitive, and indicates a sample at x=0.5, y=0.5.
		TopLeft may be simpler for internal use and linear interpolation,
		since `depth(x)` is simply `depth(floor(x)) + (x - floor(x)) * dzdx(x))`.
	"

Represents a 2.5D object using depth, slope, and material indexes for each pixel.

Ordering of data is left-to-right, then top-to-bottom, then front-to-back.
For interchange between programs, 'center' sample position should generally be used.
TopLeft is included here as an option so that programs that translate to that
internally have a way of indicating the format so as to reduce accidental confusion.


=type http://ns.nuke24.net/ShapeSheet/SurfaceMaterial
optional-attribute: title : type @ string
required-attribute: layers : type @ list of http://ns.nuke24.net/ShapeSheet/SurfaceMaterialLayer


=type http://ns.nuke24.net/ShapeSheet/SurfaceMaterial
required-attribute: glow : type @ http://ns.nuke24.net/ShapeSheet/LightColor
required-attribute: diffuse : type @ http://ns.nuke24.net/ShapeSheet/SurfaceColor
required-attribute: ruffness : type @ number
  : comment @ "0 = perfect mirror, 1 = drywall, >1 = subsurface scattering"
required-attribute: indexOfRefraction : type @ number


=type http://ns.nuke24.net/ShapeSheet/LightColor
required-attribute: http://ns.nuke24.net/ShapeSheet/Color/red
required-attribute: http://ns.nuke24.net/ShapeSheet/Color/green
required-attribute: http://ns.nuke24.net/ShapeSheet/Color/blue



=type http://ns.nuke24.net/ShapeSheet/SurfaceColor
required-attribute: http://ns.nuke24.net/ShapeSheet/Color/red
required-attribute: http://ns.nuke24.net/ShapeSheet/Color/green
required-attribute: http://ns.nuke24.net/ShapeSheet/Color/blue
required-attribute: http://ns.nuke24.net/ShapeSheet/Color/opacity

Components represent how much light is reflected, when it is
reflected (and not passed through due to opacity < 1).
