# ShapeSheet

A 2D buffer of depths (allowing multiple solid layers),
surface normals, and materials.

Plus utilities for modifying shapes and rendering them.
